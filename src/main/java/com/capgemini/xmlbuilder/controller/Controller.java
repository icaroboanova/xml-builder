package com.capgemini.xmlbuilder.controller;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.XMLOutputter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.xmlbuilder.utils.Utils;
import com.google.gson.JsonObject;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class Controller {

	@CrossOrigin
	@GetMapping("/hello")
	public ResponseEntity<String> helloWorld(){

		Element agenda = new Element("Agenda");

		Document documento = new Document(agenda);

		Element contato = new Element("Contato");
		contato.setAttribute("id", "1");

		Element nome = new Element("nome");

		nome.setText("Laura Caricchio");

		Element telefone = new Element("telefone");

		telefone.setText("73988363424");

		Element endereco = new Element("endereco");

		endereco.setText("Rua clara nunes, 543");

		Element email = new Element("email");

		email.setText("laura.caricchio@gmail.com");

		contato.addContent(nome);

		contato.addContent(telefone);

		contato.addContent(endereco);

		contato.addContent(email);

		agenda.addContent(contato);

		XMLOutputter xout = new XMLOutputter();


		try {

			xout.output(documento, System.out);

		} catch (IOException e) {

			e.printStackTrace();

		}

		try {

			FileWriter arquivo = new FileWriter(new File("C:\\Users\\iferreir\\Desktop\\teste.xml"));

			xout.output(documento, arquivo);
 
		} catch (IOException e) {

			e.printStackTrace();

		}

		return new ResponseEntity<String>("Hello world!", HttpStatus.OK);
	}
	
	@CrossOrigin
	@PostMapping("/montarxml")
	public ResponseEntity<?> montarXml(InputStream data) throws IOException{
		String resposta = "Falhou";
		
		JsonObject body = Utils.getBody(data);
		Iterator<String> keys = body.keySet().iterator();
		Element envelope = new Element("Envelope");
		Document documento = new Document(envelope);
		
		while(keys.hasNext()) {
		    String key = keys.next();
		    Element elemento = new Element(key);
		    elemento.setText(body.get(key).getAsString());
			envelope.addContent(elemento);
		}


		XMLOutputter xout = new XMLOutputter();
		
		FileWriter arquivo = new FileWriter(new File("C:\\Users\\iferreir\\Desktop\\teste2.xml"));

		xout.output(documento, arquivo);
		
		return new ResponseEntity<String>(resposta, HttpStatus.OK);
	}

}
